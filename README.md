# Simple Product Inventory System

This is a frontend application using AngularJS to post and consume data from an API

## Features

- list products
- add product
- remove product

## To-do

- search thru products