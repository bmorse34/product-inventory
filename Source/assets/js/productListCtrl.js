'use strict';

angular.module('productControllers').controller('productListCtrl', ['$scope', '$routeParams', '$location', '$timeout', 'Product', function($scope, $routeParams, $location, $timeout, Product) {

    $scope.showProducts = false;
    $scope.products = {};
    $scope.sortOrder = 'name';
    $scope.direction = '';
    $scope.query = '';
    $scope.showState = false;
    $scope.stateMessage = '';

    getProducts();

    function getProducts() {
        $scope.showProducts = false;
        Product.getProducts($scope.sortOrder, $scope.direction, $scope.query)
            .success(function (data) {

            if($location.hash() == "deleted")
                $scope.showState = true;
                $scope.stateMessage = "Product Deleted";

                $scope.products = data;
                $scope.showProducts = true;
            });
    }

    $scope.deleteProduct = function(productId) {
        Product.deleteProduct(productId)
            .success(function() {
                $scope.showState = true;
                $scope.stateMessage = "Product Deleted";

                $timeout(function(){
                    $scope.showState = false;
                }, 2000);

                getProducts();
            });
    };

}]);