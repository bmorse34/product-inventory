'use strict';

var productApp = angular.module('productDirectives', []);

productApp.directive('isNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope) {    
            scope.$watch('product.price', function(newValue,oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.product.price = oldValue;
                }
            });
        }
    };
});


productApp.directive('inputPrice', function () {
    return {
        restrict: 'EA',
        template: '<input name="{{inputName}}" ng-model="inputValue" />',
        scope: {
            inputValue: '=',
            inputName: '='
        },
        link: function (scope) {
            scope.$watch('inputValue', function(newValue,oldValue) {
                if(String(newValue).indexOf(',') != -1)
                    scope.inputValue = String(newValue).replace(',', '.');
                else {
                    var index_dot,
                        arr = String(newValue).split("");
                    if (arr.length === 0) return;
                    if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
                    if (arr.length === 2 && newValue === '-.') return;
                    if (isNaN(newValue) || ((index_dot = String(newValue).indexOf('.')) != -1 && String(newValue).length - index_dot > 3 )) {
                        scope.inputValue = oldValue;
                    }
                }
            });
        }
    };
});

productApp.directive('decimalNumber', function () {
    return {
        restrict: 'A',
        scope: {
            inputValue: '=ngModel'
        },
        link: function (scope) {
            scope.$watch('inputValue', function (newValue, oldValue) {
                if (String(newValue) == "" || typeof(newValue) == 'undefined') {
                    return;
                }
                var refresh = false;
                var val = String(newValue);
                if (val.indexOf(',') != -1) {
                    refresh = true;
                    val = val.replace(/,/g, '.');
                }

                var index_dot,
                    arr = val.split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
                if (arr.length === 2 && val === '-.') return;
                if (isNaN(val) || ((index_dot = val.indexOf('.')) != -1 && val.length - index_dot > 3 )) {
                    scope.inputValue = oldValue;
                } else if (refresh) {
                    scope.inputValue = val;
                }
            });
        }
    };
});

productApp.directive('onlyDigits', function () {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});