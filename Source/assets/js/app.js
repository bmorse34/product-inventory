'use strict';

/**
 * The contactApp module is the application core module
 *
 * @module contactApp
 */
var productApp = angular.module('productApp', [
    'ngRoute',
    'productControllers',
    'productServices',
    'productDirectives'
]);

/**
 * Configuration block for the contactApp module
 *
 * @module contactApp
 */
productApp.config(['$routeProvider',
    function($roueProvider) {
        $roueProvider.
            when('/products', {
                templateUrl: 'partials/product-list.html',
                controller: 'productListCtrl'
            }).
            when('/products/create', {
                templateUrl: 'partials/product-create.html',
                controller: 'productCreateCtrl'
            }).
            when('/products/:productId', {
                templateUrl: 'partials/product-detail.html',
                controller: 'productDetailCtrl'
            }).
            when('/products#deleted', {
                redirectTo: '/products#deleted'
            }).
            otherwise({
                redirectTo: '/products'
            });
    }
]);