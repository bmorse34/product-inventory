'use strict';

angular.module('productControllers').controller('productDetailCtrl', ['$scope', '$routeParams', '$location', '$timeout', 'Product', function($scope, $routeParams, $location, $timeout, Product) {

    $scope.product = {};
    $scope.showErrors = false;
    $scope.stateMessage = '';

    getProduct();

    function getProduct() {
        Product.getProduct($routeParams.productId)
            .success(function(data) {
                // console.log(data);
                $scope.product = data;
                // $scope.product = data[0];
            });
    }

    $scope.editProduct = function() {
        Product.editProduct($scope.product)
            .success(function(data) {
                $scope.product = data;
                $scope.showErrors = false;
                // alert('Product Saved!');
                $scope.showState = true;
                $scope.stateMessage = "Product Updated";

                $timeout(function(){
                    $scope.showState = false;
                }, 2000);
            })

            .error(function(data) {
                console.log(data);
                $scope.errors = data;
                $scope.showErrors = true;
            });
    };

    $scope.deleteProduct = function() {
        Product.deleteProduct($routeParams.productId)
            .success(function() {
                $location.path("/#/products#deleted");

                $scope.showState = true;
                $scope.stateMessage = "Product Deleted";

                // $scope.startFade = true;
                $timeout(function(){
                    $scope.showState = false;
                }, 2000);            
            });
    };

}]);