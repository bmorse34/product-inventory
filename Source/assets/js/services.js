'use strict';

angular.module('productServices', [])

.service('Product', ['$http', function($http) {

    var urlBase = 'http://angular-contact-api.dev:8000/api/products';
    // var urlBase = 'data/products.json';

    this.getProducts = function() {
        return $http.get(urlBase);
    };

    this.getProduct = function(id) {
        return $http.get(urlBase + '/' + id);
    };

    this.insertProduct = function(product) {
        return $http.post(urlBase, product);
    };

    this.editProduct = function(product) {
        return $http.put(urlBase + '/' + product.id, product);
    };

    this.deleteProduct = function(id) {
        return $http.delete(urlBase + '/' + id);
    };
}]);