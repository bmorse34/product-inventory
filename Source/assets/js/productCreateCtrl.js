'use strict';

angular.module('productControllers').controller('productCreateCtrl', ['$scope', '$location', 'Product', function($scope, $location, Product) {

    $scope.product = {};
    $scope.showErrors = false;


    $scope.insertProduct = function() {
        Product.insertProduct($scope.product)
            .success(function() {
                $location.path("/products");
            })
            .error(function(data) {
                console.log(data);
                $scope.errors = data;
                $scope.showErrors = true;
            });
    };

}]);